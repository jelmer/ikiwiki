[[!meta redir=ikiwiki/formatting delay=10]]
[[!meta robots="noindex, follow"]]

This page has moved to [[ikiwiki/formatting|ikiwiki/formatting]]. Please
update your links, as this redirection page will be removed in a future
ikiwiki release.
