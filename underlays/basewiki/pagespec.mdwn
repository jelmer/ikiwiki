[[!meta redir=ikiwiki/pagespec delay=10]]
[[!meta robots="noindex, follow"]]

This page has moved to [[ikiwiki/pagespec|ikiwiki/pagespec]]. Please update
your links, as this redirection page will be removed in a future ikiwiki
release.
