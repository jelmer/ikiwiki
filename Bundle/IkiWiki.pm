package Bundle::IkiWiki;

$VERSION = '0.01';

1;

__END__

=head1 NAME

Bundle::IkiWiki - core modules that ikiwiki needs

=head1 SYNOPSIS

perl -MCPAN -e 'install Bundle::IkiWiki'

=head1 CONTENTS

XML::Simple
Text::Markdown
Date::Parse
HTML::Template
HTML::Scrubber
CGI
CGI::FormBuilder
CGI::Session
Mail::Sendmail
HTML::Parser
URI
Data::Dumper

=head1 AUTHOR

Joey Hess <joey@ikiwiki.info>
