(Note: feel free to say "not a bug" or offer a workaround rather than changing ikiwiki.)

As reported by a Windows user trying ikiwiki: because Windows doesn't support filenames with colons, he couldn't check out the ikiwiki svn repository.  More generally, ikiwiki doesn't encode colons in filenames for wiki pages, but to support Windows users perhaps it should.

Windows does not support filenames containing any of these characters: `/ \ * : ? " < > |`

> I take it this is a problem when checking out a wiki in windows, not when
> browsing to urls that have colons in them from windows? --[[Joey]]

>> Correct.  You can't directly check out a wiki's repository from Windows if it includes filenames with those characters; you will get errors on those filenames.