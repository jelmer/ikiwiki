How about adding ACL? So that you can control which users are allowed
to read, write certain pages. The moinmoin wiki has that, and it is
something, that I think is very valuable.

> ikiwiki currently has only the most rudimentary access controls: pages
> can be locked, or unlocked and only the admin can edit locked pages. That
> could certianly be expanded on, although it's not an area that I have an
> overwhelming desire to work on myself right now. Patches appreciated and
> I'll be happy to point you in the right directions.. --[[Joey]]

>> I'm really curious how you'd suggest implementing ACLs on reading a page.
>> It seems to me the only way you could do it is .htaccess DenyAll or something,
>> and then route all page views through ikiwiki.cgi. Am I missing something?
>> --[[Ethan]]

>>> Or you could just use apache or whatever and set up the access controls
>>> there. Of course, that wouldn't integrate very well with the wiki,
>>> unless perhaps you decided to use http basic authentication and the
>>> httpauth plugin for ikiwiki that integrates with that.. --[[Joey]]

>>>> Which would rule out openid, or other fun forms of auth. And routing all access
>>>> through the CGI sort of defeats the purpose of ikiwiki. --[[Ethan]]

Also see [[!debbug 443346]].

I am considering giving this a try, implementing it as a module.
Here is how I see it:

  * a new preprocessor directive allows to define ACL entries providing permissions
    for a given (user, page, operation), as in:

    <pre>
    \[[!acl user=joe page=*.png allow=upload]]
    \[[!acl user=bob page=/blog/bob/* allow=*]]
    \[[!acl user=* page=/blog/bob/* deny=*]]
    \[[!acl user=http://jeremie.koenig.myopenid.com/ page=/todo/* deny=create
           reason="spends his time writing todo items instead of source code"]]
    </pre>

    Each would expand to a description of the resulting rule.

  * a configurable page of the wiki would be used as an ACL list.
    Possibly could refer to other ACL pages, as in:

    <pre>
    \[[!acl user=* page=/subsite/* acl=/subsite/acl.mdwn]]
    </pre>
