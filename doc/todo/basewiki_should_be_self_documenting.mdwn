The pages in the basewiki should be fully self-documenting as far as what
users need to know to edit pages in the wiki. [[ikiwiki/Formatting]]
documents the basics, but doesn't include every preprocessor directive.

Note that there's a disctinction between being self-documenting for users,
and being complete documentation for ikiwiki. The basewiki is _not_
intended to be the latter, so it lacks the usage page, all the plugin
pages, etc.

I've made some progress toward making the basewiki self-documenting by moving
the docs about using templates, shortcuts, and blogs from the plugin pages, 
onto the pages in the basewiki.

Here are some of the things that are not documented in full in the
basewiki:

	joey@kodama:~/src/ikiwiki/doc/plugins>grep usage *
	aggregate.mdwn:## usage
	graphviz.mdwn:page.  Example usage:
	graphviz.mdwn:amounts of processing time and disk usage.
	img.mdwn:## usage
	linkmap.mdwn:set of pages in the wiki. Example usage:
	map.mdwn:This plugin generates a hierarchical page map for the wiki. Example usage:
	postsparkline.mdwn:# usage
	sparkline.mdwn:# usage
	sparkline.mdwn:more detail in [its wiki](http://sparkline.wikispaces.com/usage).
	table.mdwn:## usage

Meta is another one.

See also: [[Conditional_Underlay_Files]]
