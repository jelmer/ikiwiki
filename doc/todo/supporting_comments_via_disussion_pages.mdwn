I would love to see more traditional support for comments in ikiwiki.   One
way would be to structure data on the discussion page in such a way that a
"comment" plugin could parse it and yet the discussion page would still be
a valid and usable wiki page.

For example if the discussion page looked like this:

    # Subject of First Comment
    Posted by [Adam Shand](http://adam.shand.net/) at 10:34PM on 14/04/2007

    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi consectetuer nunc quis 
    magna.  Etiam non est eget sapien vulputate varius. Vivamus magna. Sed justo. Donec 
    pellentesque ultrices urna.

    # Subject of the Second Comment
    Posted by [Foo Bar](http://foobar.net/) at 11:41PM on 14/04/2007

    Quisque lacinia, lorem eget ornare facilisis, enim eros iaculis felis, id volutpat nibh
    mauris ut felis. Vestibulum risus nibh, adipiscing volutpat, volutpat et, lacinia ut, 
    pede. Maecenas dolor. Vivamus feugiat volutpat ligula.

Each header marks the start of a new comment and the line immediately
following is the comments meta data (author, email/url, datestamp).
Hopefully you could structure it in such a way that the scope 

This would allow:

 * A comment plugin to render the comments in "traditional blog" format .  
 * Possibly even support nesting comments by the header level?
 * A comment plugin to create a form at the bottom of the page for people to add comments in the appropriate format to the discussion page
 * Still remain usable and readable by people who work via svn.
 * When there is ACL support you could mark the discussion page as read only so it could only be updated by the comment plugin (if that's what you wanted)

Is this simple enough to be sensible?

-- [[AdamShand]]

> Well, if it's going to look like a blog, why not store the data the same
> way ikiwiki stores blogs, with a separate page per comment? As already
> suggested in [[discussion_page_as_blog]] though there are some things to
> be worked out also discussed there.
> --[[Joey]]

>> I certainly won't be fussy about how it gets implemented, I was just trying to think of the lightest weight most "wiki" solution.  :-) -- Adam.

>>> As a side note, the feature described above (having a form not to add a page but to expand it in a formated way) would be useful for other things when the content is short (timetracking, sub-todo list items, etc..) --[[hb]]

I've been looking into this.  I'd like to implement a "blogcomments"
plugin.  Looking at the code, I think the way to go is to have a
formbuilder_setup hook that uses a different template instead of the
standard editpage one.  That template would not display the editcontent
field.  The problem that I'm running into is that I need to append the new
content to the old one.

-- [[MarceloMagallon]]

> Anything I can do to help? --[[Joey]]

>> Figured it out.  Can you comment on the code below?  Thanks. -- [[MarceloMagallon]]

So, I have some code, included below.  For some reason that I don't quite get it's not updating the wiki page after a submit.  Maybe it's something silly on my side...

What I ended up doing is write something like this to the page:

    [[!blogcomment from="""Username""" timestamp="""12345""" subject="""Some text""" text="""the text of the comment"""]]

Each comment is processed to something like this:

    <div>
        <dl>
            <dt>From</dt><dd>Username</dd>
            <dt>Date</dt><dd>Date (needs fixing)</dd>
            <dt>Subject</dt><dd>Subject text</dd>
        </dl>

        <p>Text of the comment...</p>
    </div>

.  In this way the comments can be styled using CSS.

-- [[MarceloMagallon]]

# Code

    #!/usr/bin/perl
    package IkiWiki::Plugin::comments;

    use warnings;
    use strict;
    use IkiWiki '1.02';

    sub import { #{{{
        hook(type => "formbuilder_setup", id => "comments",
            call => \&formbuilder_setup);
        hook(type => "preprocess", id => "blogcomment",
            call => \&preprocess);  
    } # }}}

    sub formbuilder_setup (@) { #{{{
        my %params=@_;
        my $cgi = $params{cgi};
        my $form = $params{form};   
        my $session = $params{session};

        my ($page)=$form->field('page');
        $page=IkiWiki::titlepage(IkiWiki::possibly_foolish_untaint($page));

        # XXX: This needs something to make it blog specific
        unless ($page =~ m{/discussion$} &&
                $cgi->param('do') eq 'edit' &&
                ! exists $form->{title})
        {
            return;
        }

        if (! $form->submitted)
        {
            $form->template(IkiWiki::template_file("makeblogcomment.tmpl"));
            $form->field(name => "blogcomment", type => "textarea", rows => 20,
                cols => 80);
            return;
        }

        my $content="";
        if (exists $pagesources{$page}) {
            $content=readfile(srcfile($pagesources{$page}));
            $content.="\n\n";
        }
        my $name=defined $session->param('name') ?
            $session->param('name') : gettext('Anonymous');
        my $timestamp=time;
        my $subject=defined $cgi->param('comments') ?
            $cgi->param('comments') : '';
        my $comment=$cgi->param('blogcomment');

        $content.=qq{[[!blogcomment from="""$name""" timestamp="""$timestamp""" subject="""$subject""" text="""$comment"""]]\n\n};
        $content=~s/\n/\r\n/g;
        $form->field(name => "editcontent", value => $content, force => 1);
    } # }}}

    sub preprocess (@) { #{{{
        my %params=@_;

        my ($text, $date, $from, $subject, $r);

        $text=IkiWiki::preprocess($params{page}, $params{destpage},
                IkiWiki::filter($params{page}, $params{text}));
        $from=exists $params{from} ? $params{from} : gettext("Anonymous");
        $date=localtime($params{timestamp}) if exists $params{timestamp};
        $subject=$params{subject} if exists $params{subject};

        $r = qq{<div class="blogcomment"><dl>\n};
        $r .= '<dt>' . gettext("From") . "</dt><dd>$from</dd>\n" if defined $from;
        $r .= '<dt>' . gettext("Date") . "</dt><dd>$date</dd>\n" if defined $date;
        $r .= '<dt>' . gettext("Subject") . "</dt><dd>$subject</dd>\n"
            if defined $subject;
        $r .= "</dl>\n" . $text . "</div>\n";

        return $r;
    } # }}}
    
    1;