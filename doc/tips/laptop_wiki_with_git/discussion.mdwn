I have followed this idea along, and it seems to work pretty well. 
Now I have a question as a git newbie. Can I have the post-commit hook on the server use something like rsync to update the files on a third machine hosting the web server?  The web server does not have git (cretins!). Of course I could just run a cron job.

Or, was this last remark about rebuilding after pulling meant to apply to rebuilding after pushing as well?
[[DavidBremner]]

* *Updated*  Now that I play with this a bit, this seems not so important.  Having a seperate sync operation that I run from the laptop is no big deal, and lets me update the parts of my site not yet managed by ikiwiki at the same time.
