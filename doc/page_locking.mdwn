The administrator of a wiki can choose to lock some pages, which allows
only the admin to edit them using the online interface. This doesn't
prevent anyone who can commit to the underlying revision control system
from editing the pages, however.

To lock a page, log into the wiki as whatever user is configured as the
admin, and in your Preferences page, you'll find a field listing locked
pages. This is a [[ikiwiki/PageSpec]], so you have a fair bit of control
over what kinds of pages to lock. For example, you could choose to lock all
pages created before 2006, or all pages that are linked to from the page
named "locked". More usually though, you'll just list some names of pages
to lock.

One handy thing to do if you're using ikiwiki for your blog is to lock 
"* and !*/Discussion". This prevents others from adding to or modifying
posts in your blog, while still letting them comment via the Discussion
pages.
