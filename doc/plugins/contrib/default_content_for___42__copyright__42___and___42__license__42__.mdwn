[[!template id=plugin name=copyright author="[[tschwinge]]"]]
[[!template id=plugin name=license author="[[tschwinge]]"]]

[[!meta title="default content for *copyright* and *license*"]]

Someone was just asking for it and I had written these two plugins already some months ago,
so I'm now publishing them here.

<http://www.schwinge.homeip.net/~thomas/tmp/copyright.pm>
<http://www.schwinge.homeip.net/~thomas/tmp/license.pm>

/!\ Note that there is still an issue to be resolved with these plugins: have a look at
[tmpfs](http://www.bddebian.com/~wiki/hurd/translator/tmpfs/).  There are two inlined pages,
[tmpfs-notes\_bing](http://www.bddebian.com/~wiki/hurd/translator/tmpfs/notes_bing/) and
[tmpfs-notes\_various](http://www.bddebian.com/~wiki/hurd/translator/tmpfs/notes_various/).
Compare the *copyright* and *license* information of the inlined pages on *tmpfs* to those
on the source pages, *tmpfs-notes_bing* and *tmpfs-notes_various*.
They are different, but shouldn't be.

--[[tschwinge]]

I was asking about this in IRC the other day, but someone pointed me at the
[[Varioki|todo/varioki_--_add_template_variables___40__with_closures_for_values__41___in_ikiwiki.setup]]
plugin. It seems to me that it would be a better idea to have a way of defining
template variables in general, rather than having to add a new plugin for every
template variable somebody wants to use.

--[[bma]]

Copyright and license values are not "template values", they are values
tracked by the meta plugin, and that various code compares and uses to fill
out the templates. Something like varioki cannot do that. --[[Joey]]
