[[!template id=plugin name=shortcut author="[[Joey]]"]]
[[!tag type/format]]

This plugin allows external links to commonly linked to sites to be made
more easily using shortcuts.

The available shortcuts are defined on the [[shortcuts]] page in
the wiki.
