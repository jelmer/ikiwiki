[[!template id=plugin name=map author="Alessandro Dotti Contra"]]
[[!tag type/meta]]

[[!meta description="some page description"]]

This plugin generates a hierarchical page map for the wiki. Example usage:

	\[[!map pages="* and !blog/* and !*/Discussion"]]

If the pages to include are not specified, all pages (and other files) in
the wiki are mapped.

By default, the names of pages are shown in the map. The `show` parameter
can be used to show the titles or descriptions of pages instead (as set by
the [[meta]] plugin). For example:

	\[[!map pages="* and !blog/* and !*/Discussion" show=title]]

Hint: To limit the map to displaying pages less than a certain level deep,
use a [[ikiwiki/PageSpec]] like this: `pages="* and !*/*/*"`

[[!if test="enabled(map)" then="""
Here's an example map, for the plugins section of this wiki:

[[!map pages="(plugins or plugins/*) and !*/*/*"]]
"""]]
