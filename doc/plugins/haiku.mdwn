[[!template id=plugin name=haiku author="[[Joey]]"]]
[[!tag type/fun]]

This plugin allows inserting a randomly generated haiku into a wiki page.
Just type:

	\[[!haiku hint="argument"]]

[[!haiku hint="argument test"]]

The hint parameter can be omitted, it only provides the generator a hint of
what to write the haiku about. If no hint is given, it might base it on the
page name. Since the vocabulary it knows is very small, many hints won't
affect the result at all.

As a special bonus, enabling this plugin makes any error messages ikiwiki
should display be written in haiku.

You need to have the Coy module installed for this plugin to do anything
interesting. That does all the heavy lifting.
