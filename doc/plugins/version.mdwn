[[!template id=plugin name=version author="[[Joey]]"]]
[[!tag type/useful]]

This plugin allows inserting the version of ikiwiki onto a page.

Whenever ikiwiki is upgraded to a new version, the page will be rebuilt,
updating the version number.

Use is simple:

	\[[!version ]]
