# Czech translation of ikiwiki.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ikiwiki package.
# Miroslav Kure <kurem@debian.cz>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: ikiwiki\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-02-03 14:52-0500\n"
"PO-Revision-Date: 2007-05-09 21:21+0200\n"
"Last-Translator: Miroslav Kure <kurem@debian.cz>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../IkiWiki/CGI.pm:125
msgid "You need to log in first."
msgstr "Nejprve se musíte přihlásit."

#: ../IkiWiki/CGI.pm:155
msgid "login failed, perhaps you need to turn on cookies?"
msgstr "přihlášení selhalo; možná si musíte povolit cookies?"

#: ../IkiWiki/CGI.pm:184
msgid "Login"
msgstr "Přihlášení"

#: ../IkiWiki/CGI.pm:185
msgid "Preferences"
msgstr "Předvolby"

#: ../IkiWiki/CGI.pm:186
msgid "Admin"
msgstr "Správce"

#: ../IkiWiki/CGI.pm:235
msgid "Preferences saved."
msgstr "Nastavení uloženo."

#: ../IkiWiki/CGI.pm:291
#, perl-format
msgid "%s is not an editable page"
msgstr "%s není editovatelná stránka"

#: ../IkiWiki/CGI.pm:382 ../IkiWiki/Plugin/brokenlinks.pm:24
#: ../IkiWiki/Plugin/inline.pm:241 ../IkiWiki/Plugin/opendiscussion.pm:17
#: ../IkiWiki/Plugin/orphans.pm:28 ../IkiWiki/Render.pm:95
#: ../IkiWiki/Render.pm:175
msgid "discussion"
msgstr "diskuse"

#: ../IkiWiki/CGI.pm:429
#, perl-format
msgid "creating %s"
msgstr "vytvářím %s"

#: ../IkiWiki/CGI.pm:447 ../IkiWiki/CGI.pm:466 ../IkiWiki/CGI.pm:476
#: ../IkiWiki/CGI.pm:510 ../IkiWiki/CGI.pm:554
#, perl-format
msgid "editing %s"
msgstr "upravuji %s"

#: ../IkiWiki/CGI.pm:643
msgid "You are banned."
msgstr "Jste vyhoštěni."

#: ../IkiWiki/Plugin/aggregate.pm:72
#, perl-format
msgid "missing %s parameter"
msgstr "chybí parametr %s"

#: ../IkiWiki/Plugin/aggregate.pm:100
msgid "new feed"
msgstr "nový zdroj"

#: ../IkiWiki/Plugin/aggregate.pm:114
msgid "posts"
msgstr "příspěvky"

#: ../IkiWiki/Plugin/aggregate.pm:116
msgid "new"
msgstr "nový"

#: ../IkiWiki/Plugin/aggregate.pm:232
#, perl-format
msgid "expiring %s (%s days old)"
msgstr "expiruji %s (stará %s dnů)"

#: ../IkiWiki/Plugin/aggregate.pm:239
#, perl-format
msgid "expiring %s"
msgstr "expiruji %s"

#: ../IkiWiki/Plugin/aggregate.pm:265
#, perl-format
msgid "processed ok at %s"
msgstr "zpracováno ok %s"

#: ../IkiWiki/Plugin/aggregate.pm:270
#, perl-format
msgid "checking feed %s ..."
msgstr "kontroluji zdroj %s ..."

#: ../IkiWiki/Plugin/aggregate.pm:275
#, perl-format
msgid "could not find feed at %s"
msgstr "nemohu najít zdroj na %s"

#: ../IkiWiki/Plugin/aggregate.pm:290
msgid "feed not found"
msgstr "zdroj nebyl nalezen"

#: ../IkiWiki/Plugin/aggregate.pm:301
#, perl-format
msgid "(invalid UTF-8 stripped from feed)"
msgstr "(neplatné UTF-8 bylo ze zdroje odstraněno)"

#: ../IkiWiki/Plugin/aggregate.pm:307
#, perl-format
msgid "(feed entities escaped)"
msgstr ""

#: ../IkiWiki/Plugin/aggregate.pm:313
msgid "feed crashed XML::Feed!"
msgstr "zdroj shodil XML::Feed!"

#: ../IkiWiki/Plugin/aggregate.pm:387
#, perl-format
msgid "creating new page %s"
msgstr "vytvářím novou stránku %s"

#: ../IkiWiki/Plugin/brokenlinks.pm:40
#, perl-format
msgid "%s from %s"
msgstr ""

#: ../IkiWiki/Plugin/brokenlinks.pm:47
msgid "There are no broken links!"
msgstr "Žádné porušené odkazy!"

#: ../IkiWiki/Plugin/conditional.pm:18
#, perl-format
msgid "%s parameter is required"
msgstr "parametr %s je vyžadován"

#: ../IkiWiki/Plugin/edittemplate.pm:41
#, fuzzy
msgid "template not specified"
msgstr "šablona %s nebyla nalezena"

#: ../IkiWiki/Plugin/edittemplate.pm:44
#, fuzzy
msgid "match not specified"
msgstr "jméno souboru s obalem nebylo zadáno"

#: ../IkiWiki/Plugin/edittemplate.pm:49
#, perl-format
msgid "edittemplate %s registered for %s"
msgstr ""

#: ../IkiWiki/Plugin/edittemplate.pm:111
#, fuzzy
msgid "failed to process"
msgstr "nepodařilo se zpracovat:"

#: ../IkiWiki/Plugin/fortune.pm:18
msgid "fortune failed"
msgstr "fortune selhal"

#: ../IkiWiki/Plugin/googlecalendar.pm:22
msgid "failed to find url in html"
msgstr "v html se nepodařilo nalézt url"

#: ../IkiWiki/Plugin/graphviz.pm:58
msgid "failed to run graphviz"
msgstr "nepodařilo se spustit graphviz"

#: ../IkiWiki/Plugin/graphviz.pm:85
msgid "prog not a valid graphviz program"
msgstr "program není platným programem graphviz"

#: ../IkiWiki/Plugin/img.pm:53
#, perl-format
msgid "bad size \"%s\""
msgstr "chybná velikost \"%s\""

#: ../IkiWiki/Plugin/img.pm:63 ../IkiWiki/Plugin/img.pm:67
#: ../IkiWiki/Plugin/img.pm:84
#, perl-format
msgid "failed to read %s: %s"
msgstr "nelze číst %s: %s"

#: ../IkiWiki/Plugin/img.pm:70
#, perl-format
msgid "failed to resize: %s"
msgstr "nelze změnit velikost: %s"

#: ../IkiWiki/Plugin/img.pm:101
#, fuzzy, perl-format
msgid "failed to determine size of image %s"
msgstr "nelze změnit velikost: %s"

#: ../IkiWiki/Plugin/inline.pm:42
msgid "Must specify url to wiki with --url when using --rss or --atom"
msgstr "Při používání --rss nebo --atom musíte pomocí --url zadat url k wiki"

#: ../IkiWiki/Plugin/inline.pm:135
#, perl-format
msgid "unknown sort type %s"
msgstr "neznámý typ řazení %s"

#: ../IkiWiki/Plugin/inline.pm:200
msgid "Add a new post titled:"
msgstr "Přidat nový příspěvek nazvaný:"

#: ../IkiWiki/Plugin/inline.pm:216
#, perl-format
msgid "nonexistant template %s"
msgstr "neexistující šablona %s"

#: ../IkiWiki/Plugin/inline.pm:249 ../IkiWiki/Render.pm:99
msgid "Discussion"
msgstr "Diskuse"

#: ../IkiWiki/Plugin/inline.pm:463
msgid "RPC::XML::Client not found, not pinging"
msgstr "RPC::XML::Client nebyl nalezen, nepinkám"

#: ../IkiWiki/Plugin/linkmap.pm:98
msgid "failed to run dot"
msgstr "nepodařilo se spustit dot"

#: ../IkiWiki/Plugin/lockedit.pm:29
#, perl-format
msgid "%s is locked by %s and cannot be edited"
msgstr "Stránka %s je zamknutá uživatelem %s a nelze ji měnit"

#: ../IkiWiki/Plugin/mdwn.pm:37
#, perl-format
msgid "failed to load Markdown.pm perl module (%s) or /usr/bin/markdown (%s)"
msgstr ""
"selhalo nahrání perlového modulu Markdown.pm (%s) nebo /usr/bin/markdown (%s)"

#: ../IkiWiki/Plugin/meta.pm:119
msgid "stylesheet not found"
msgstr "styl nebyl nalezen"

#: ../IkiWiki/Plugin/meta.pm:143
#, fuzzy
msgid "redir page not found"
msgstr "zdroj nebyl nalezen"

#: ../IkiWiki/Plugin/meta.pm:156
#, fuzzy
msgid "redir cycle is not allowed"
msgstr "zdroj nebyl nalezen"

#: ../IkiWiki/Plugin/mirrorlist.pm:23
msgid "Mirrors"
msgstr "Zrcadla"

#: ../IkiWiki/Plugin/mirrorlist.pm:23
msgid "Mirror"
msgstr "Zrcadlo"

#: ../IkiWiki/Plugin/more.pm:8
msgid "more"
msgstr "více"

#: ../IkiWiki/Plugin/openid.pm:45
msgid "Log in with"
msgstr "Přihlásit pomocí"

#: ../IkiWiki/Plugin/openid.pm:48
msgid "Get an OpenID"
msgstr "Získat OpenID"

#: ../IkiWiki/Plugin/orphans.pm:42
msgid "All pages are linked to by other pages."
msgstr "Na každou stránku vede odkaz z jiné stránky."

#: ../IkiWiki/Plugin/pagetemplate.pm:21
msgid "bad or missing template"
msgstr ""

#: ../IkiWiki/Plugin/passwordauth.pm:162
msgid "Account creation successful. Now you can Login."
msgstr "Vytvoření účtu bylo úspěšné. Nyní se můžete přihlásit."

#: ../IkiWiki/Plugin/passwordauth.pm:165
msgid "Error creating account."
msgstr "Chyba při vytváření účtu."

#: ../IkiWiki/Plugin/passwordauth.pm:186
msgid "Failed to send mail"
msgstr "Nepodařilo se odeslat email."

#: ../IkiWiki/Plugin/passwordauth.pm:188
msgid "Your password has been emailed to you."
msgstr "Vaše heslo vám bylo zasláno."

#: ../IkiWiki/Plugin/poll.pm:64
msgid "vote"
msgstr "hlasovat"

#: ../IkiWiki/Plugin/poll.pm:72
msgid "Total votes:"
msgstr "Celkem hlasů:"

#: ../IkiWiki/Plugin/polygen.pm:32
msgid "polygen not installed"
msgstr "polygen není nainstalován"

#: ../IkiWiki/Plugin/polygen.pm:51
msgid "polygen failed"
msgstr "polygen selhal"

#: ../IkiWiki/Plugin/postsparkline.pm:32
msgid "missing formula"
msgstr "chybí vzorec"

#: ../IkiWiki/Plugin/postsparkline.pm:39
msgid "unknown formula"
msgstr "neznámý vzorec"

#. translators: These descriptions of times of day are used
#. translators: in messages like "last edited <description>".
#. translators: %A is the name of the day of the week, while
#. translators: %A- is the name of the previous day.
#: ../IkiWiki/Plugin/prettydate.pm:15
msgid "late %A- night"
msgstr "%A- pozdě v noci"

#: ../IkiWiki/Plugin/prettydate.pm:17
msgid "in the wee hours of %A- night"
msgstr "%A- pozdě v noci, spíše brzké ranní hodiny"

#: ../IkiWiki/Plugin/prettydate.pm:20
msgid "terribly early %A morning"
msgstr "%A příšerně brzo ráno"

#: ../IkiWiki/Plugin/prettydate.pm:22
msgid "early %A morning"
msgstr "%A brzo ráno"

#: ../IkiWiki/Plugin/prettydate.pm:25
msgid "mid-morning %A"
msgstr "%A dopoledne"

#: ../IkiWiki/Plugin/prettydate.pm:26
msgid "late %A morning"
msgstr "%A pozdě dopoledne"

#: ../IkiWiki/Plugin/prettydate.pm:27
msgid "at lunch time on %A"
msgstr "%A kolem oběda"

#: ../IkiWiki/Plugin/prettydate.pm:29
msgid "%A afternoon"
msgstr "%A odpoledne"

#: ../IkiWiki/Plugin/prettydate.pm:32
msgid "late %A afternoon"
msgstr "%A pozdě odpoledne"

#: ../IkiWiki/Plugin/prettydate.pm:33
msgid "%A evening"
msgstr "%A večer"

#: ../IkiWiki/Plugin/prettydate.pm:35
msgid "late %A evening"
msgstr "%A pozdě večer"

#: ../IkiWiki/Plugin/prettydate.pm:37
msgid "%A night"
msgstr "%A v noci"

#: ../IkiWiki/Plugin/prettydate.pm:78
msgid "at teatime on %A"
msgstr "%A během odpoledního čaje"

#: ../IkiWiki/Plugin/prettydate.pm:82
msgid "at midnight"
msgstr "o půlnoci"

#: ../IkiWiki/Plugin/prettydate.pm:85
msgid "at noon on %A"
msgstr "%A o poledni"

#: ../IkiWiki/Plugin/recentchanges.pm:74
#, fuzzy
msgid "missing page"
msgstr "chybí hodnoty"

#: ../IkiWiki/Plugin/recentchanges.pm:76
#, perl-format
msgid "The page %s does not exist."
msgstr ""

#: ../IkiWiki/Plugin/search.pm:34
#, perl-format
msgid "Must specify %s when using the search plugin"
msgstr "Při používání vyhledávacího modulu musíte zadat %s"

#: ../IkiWiki/Plugin/search.pm:58
msgid "cleaning hyperestraier search index"
msgstr "čistím vyhledávací index hyperestraieru"

#: ../IkiWiki/Plugin/search.pm:64
msgid "updating hyperestraier search index"
msgstr "aktualizuji vyhledávací index hyperestraieru"

#: ../IkiWiki/Plugin/shortcut.pm:18
msgid "shortcut plugin will not work without a shortcuts.mdwn"
msgstr ""

#: ../IkiWiki/Plugin/shortcut.pm:27
msgid "missing name or url parameter"
msgstr "chybí parametr jméno nebo url"

#. translators: This is used to display what shortcuts are defined.
#. translators: First parameter is the name of the shortcut, the second
#. translators: is an URL.
#: ../IkiWiki/Plugin/shortcut.pm:36
#, perl-format
msgid "shortcut %s points to <i>%s</i>"
msgstr "zkratka %s odkazuje na <i>%s</i>"

#: ../IkiWiki/Plugin/smiley.pm:23
msgid "failed to parse any smileys"
msgstr "nepodařilo se rozpoznat žádné smajlíky"

#: ../IkiWiki/Plugin/sparkline.pm:63
msgid "parse error"
msgstr "chyba rozpoznávání"

#: ../IkiWiki/Plugin/sparkline.pm:69
msgid "bad featurepoint diameter"
msgstr "chybný průměr zvýrazněného bodu (featurepoint)"

#: ../IkiWiki/Plugin/sparkline.pm:79
msgid "bad featurepoint location"
msgstr "chybné umístění zvýrazněného bodu (featurepoint)"

#: ../IkiWiki/Plugin/sparkline.pm:90
msgid "missing values"
msgstr "chybí hodnoty"

#: ../IkiWiki/Plugin/sparkline.pm:95
msgid "bad height value"
msgstr "chybná výška"

#: ../IkiWiki/Plugin/sparkline.pm:102
msgid "missing width parameter"
msgstr "chybí parametr šířka (width)"

#: ../IkiWiki/Plugin/sparkline.pm:106
msgid "bad width value"
msgstr "chybná šířka"

#: ../IkiWiki/Plugin/sparkline.pm:144
msgid "failed to run php"
msgstr "nepodařilo se spustit php"

#: ../IkiWiki/Plugin/table.pm:22
msgid "cannot find file"
msgstr "nemohu najít soubor"

#: ../IkiWiki/Plugin/table.pm:64
msgid "unknown data format"
msgstr "neznámý formát dat"

#: ../IkiWiki/Plugin/table.pm:72
msgid "empty data"
msgstr "prázdná data"

#: ../IkiWiki/Plugin/table.pm:92
msgid "Direct data download"
msgstr "Stáhnout zdrojová data"

#: ../IkiWiki/Plugin/table.pm:126
#, perl-format
msgid "parse fail at line %d: %s"
msgstr "zpracovávání selhalo na řádku %d: %s"

#: ../IkiWiki/Plugin/template.pm:19
msgid "missing id parameter"
msgstr "chybí parametr id"

#: ../IkiWiki/Plugin/template.pm:26
#, perl-format
msgid "template %s not found"
msgstr "šablona %s nebyla nalezena"

#: ../IkiWiki/Plugin/template.pm:45
msgid "failed to process:"
msgstr "nepodařilo se zpracovat:"

#: ../IkiWiki/Plugin/teximg.pm:30
#, fuzzy
msgid "missing tex code"
msgstr "chybí hodnoty"

#: ../IkiWiki/Plugin/teximg.pm:37
msgid "code includes disallowed latex commands"
msgstr ""

#: ../IkiWiki/Plugin/teximg.pm:96
#, fuzzy
msgid "failed to generate image from code"
msgstr "nelze změnit velikost: %s"

#: ../IkiWiki/Plugin/toggle.pm:88
msgid "(not toggleable in preview mode)"
msgstr ""

#: ../IkiWiki/Rcs/Stub.pm:62
msgid "getctime not implemented"
msgstr "getctime není implementováno"

#: ../IkiWiki/Render.pm:273 ../IkiWiki/Render.pm:294
#, perl-format
msgid "skipping bad filename %s"
msgstr "přeskakuji chybné jméno souboru %s"

#: ../IkiWiki/Render.pm:343
#, perl-format
msgid "removing old page %s"
msgstr "odstraňuji starou stránku %s"

#: ../IkiWiki/Render.pm:384
#, perl-format
msgid "scanning %s"
msgstr "prohledávám %s"

#: ../IkiWiki/Render.pm:389
#, perl-format
msgid "rendering %s"
msgstr "zpracovávám %s"

#: ../IkiWiki/Render.pm:410
#, perl-format
msgid "rendering %s, which links to %s"
msgstr "zpracovávám %s, která odkazuje na %s"

#: ../IkiWiki/Render.pm:431
#, perl-format
msgid "rendering %s, which depends on %s"
msgstr "zpracovávám %s, která závisí na %s"

#: ../IkiWiki/Render.pm:470
#, perl-format
msgid "rendering %s, to update its backlinks"
msgstr "zpracovávám %s, aby se aktualizovaly zpětné odkazy"

#: ../IkiWiki/Render.pm:482
#, perl-format
msgid "removing %s, no longer rendered by %s"
msgstr "odstraňuji %s, již není zpracovávána %s"

#: ../IkiWiki/Render.pm:508
#, perl-format
msgid "ikiwiki: cannot render %s"
msgstr "ikiwiki: nelze zpracovat %s"

#. translators: The first parameter is a filename, and the second
#. translators: is a (probably not translated) error message.
#: ../IkiWiki/Setup.pm:15
#, perl-format
msgid "cannot read %s: %s"
msgstr "nemohu číst %s: %s"

#: ../IkiWiki/Setup/Standard.pm:32
msgid "generating wrappers.."
msgstr "generuji obaly..."

#: ../IkiWiki/Setup/Standard.pm:72
msgid "rebuilding wiki.."
msgstr "znovu vytvářím wiki..."

#: ../IkiWiki/Setup/Standard.pm:75
msgid "refreshing wiki.."
msgstr "obnovuji wiki..."

#: ../IkiWiki/Setup/Standard.pm:84
msgid "done"
msgstr "hotovo"

#: ../IkiWiki/Wrapper.pm:16
#, perl-format
msgid "%s doesn't seem to be executable"
msgstr "%s není spustitelný soubor"

#: ../IkiWiki/Wrapper.pm:20
msgid "cannot create a wrapper that uses a setup file"
msgstr "nemohu vytvořit obal, který využívá soubor setup"

#: ../IkiWiki/Wrapper.pm:24
msgid "wrapper filename not specified"
msgstr "jméno souboru s obalem nebylo zadáno"

#. translators: The first parameter is a filename, and the second is
#. translators: a (probably not translated) error message.
#: ../IkiWiki/Wrapper.pm:48
#, perl-format
msgid "failed to write %s: %s"
msgstr "nelze zapsat %s: %s"

#. translators: The parameter is a C filename.
#: ../IkiWiki/Wrapper.pm:99
#, perl-format
msgid "failed to compile %s"
msgstr "nelze zkompilovat %s"

#. translators: The parameter is a filename.
#: ../IkiWiki/Wrapper.pm:119
#, perl-format
msgid "successfully generated %s"
msgstr "%s byl úspěšně vytvořen"

#: ../ikiwiki.in:13
msgid "usage: ikiwiki [options] source dest"
msgstr "použití: ikiwiki [volby] zdroj cíl"

#: ../ikiwiki.in:81
msgid "usage: --set var=value"
msgstr ""

#: ../IkiWiki.pm:127
msgid "Must specify url to wiki with --url when using --cgi"
msgstr "Při použití --cgi musíte pomocí --url zadat url k wiki"

#: ../IkiWiki.pm:196 ../IkiWiki.pm:197
msgid "Error"
msgstr "Chyba"

#. translators: The first parameter is a
#. translators: preprocessor directive name,
#. translators: the second a page name, the
#. translators: third a number.
#: ../IkiWiki.pm:750
#, perl-format
msgid "%s preprocessing loop detected on %s at depth %i"
msgstr "Byla rozpoznána smyčka direktivy %s na %s v hloubce %i"

#, fuzzy
#~ msgid ""
#~ "REV is not set, not running from mtn post-commit hook, cannot send "
#~ "notifications"
#~ msgstr ""
#~ "REV není nastavena, není spuštěna ze svn post-commit, nemohu zaslat "
#~ "oznámení"

#, fuzzy
#~ msgid "REV is not a valid revision identifier, cannot send notifications"
#~ msgstr ""
#~ "REV není nastavena, není spuštěna ze svn post-commit, nemohu zaslat "
#~ "oznámení"

#~ msgid ""
#~ "REV is not set, not running from svn post-commit hook, cannot send "
#~ "notifications"
#~ msgstr ""
#~ "REV není nastavena, není spuštěna ze svn post-commit, nemohu zaslat "
#~ "oznámení"

#~ msgid "update of %s's %s by %s"
#~ msgstr "aktualizace %s (%s) uživatelem %s"

#~ msgid "%s not found"
#~ msgstr "%s nenalezen"

#~ msgid "What's this?"
#~ msgstr "Co to je?"

#~ msgid "(use FirstnameLastName)"
#~ msgstr "(použijte KřestníJménoPříjmení)"
